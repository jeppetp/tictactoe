import { Elm } from "./Main.elm";

const app = Elm.Main.init({
  node: document.getElementById("app"),
})


function connect(socketUrl) {
  // Create the WebSocket.
  var socket = new WebSocket(socketUrl);

  // When a command goes to the `sendMessage` port, we pass the message
  // along to the WebSocket.
  app.ports.sendMessage.subscribe(function(message) {
    console.log(message)
    socket.send(message);
  });

  // When a message comes into our WebSocket, we pass the message along
  // to the `messageReceiver` port.
  socket.addEventListener("message", function(event) {
    console.log(event)
    app.ports.messageReceiver.send(event.data);
  });
}

app.ports.connectToSocket.subscribe(connect)
