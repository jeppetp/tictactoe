module Svgs exposing (crossSvg, noughtSvg)

import Html exposing (..)
import Svg exposing (..)
import Svg.Attributes exposing (..)


crossSvg : Html msg
crossSvg =
    svg
        [ width "100"
        , height "100"
        ]
        [ g
            [ fill "white"
            , stroke "black"
            , strokeWidth "8"
            , transform "rotate(45, 50, 50) scale(.82)"
            ]
            [ rect
                [ width "80"
                , height "20"
                , x "10"
                , y "40"
                ]
                []
            , rect
                [ width "20"
                , height "80"
                , x "40"
                , y "10"
                ]
                []
            , rect
                [ width "78"
                , height "18"
                , x "11"
                , y "41"
                , stroke "none"
                ]
                []
            , rect
                [ width "18"
                , height "78"
                , x "41"
                , y "11"
                , stroke "none"
                ]
                []
            ]
        ]


noughtSvg : Html msg
noughtSvg =
    svg
        [ width "100"
        , height "100"
        , transform ""
        ]
        [ circle
            [ cx "61.5"
            , cy "46"
            , r "30"
            , fill "none"
            , stroke "black"
            , strokeWidth "20"
            , transform "scale(.82)"
            ]
            []
        ]
