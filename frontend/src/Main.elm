port module Main exposing (..)

import Browser
import Browser.Navigation
import Element as El exposing (Color, clip, rgb255)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html)
import Html.Attributes as Hat
import Json.Decode as JD
import Json.Encode as JE
import Svgs exposing (crossSvg, noughtSvg)
import Time exposing (Posix)
import Url



-- Main


onUrlChange : Url.Url -> Msg
onUrlChange _ =
    Noop


onUrlRequest : Browser.UrlRequest -> Msg
onUrlRequest _ =
    Noop


main : Program () Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlChange = onUrlChange
        , onUrlRequest = onUrlRequest
        }



-- Ports


port sendMessage : String -> Cmd msg


port messageReceiver : (String -> msg) -> Sub msg


port connectToSocket : String -> Cmd msg


moveDecoder : JD.Decoder Move
moveDecoder =
    JD.map3 Move
        (JD.field "field" JD.string |> JD.andThen fieldHelp)
        (JD.field "value" JD.string |> JD.andThen fieldValueHelp)
        (JD.field "timeSpent" JD.int)


fieldEncoder : Field -> JE.Value
fieldEncoder field =
    case field of
        NorthWest ->
            JE.string "NorthWest"

        North ->
            JE.string "North"

        NorthEast ->
            JE.string "NorthEast"

        West ->
            JE.string "West"

        Center ->
            JE.string "Center"

        East ->
            JE.string "East"

        SouthWest ->
            JE.string "SouthWest"

        South ->
            JE.string "South"

        SouthEast ->
            JE.string "SouthEast"


fieldValueEncoder : FieldValue -> JE.Value
fieldValueEncoder fieldValue =
    case fieldValue of
        Cross ->
            JE.string "Cross"

        Nought ->
            JE.string "Nought"

        Empty ->
            JE.string "Empty"


moveEncoder : Move -> String
moveEncoder move =
    let
        encodeObject =
            JE.object
                [ ( "field", fieldEncoder move.field )
                , ( "value", fieldValueEncoder move.value )
                , ( "timeSpent", JE.int move.timeSpent )
                ]
    in
    JE.encode 0 encodeObject


fieldHelp : String -> JD.Decoder Field
fieldHelp fieldString =
    case fieldString of
        "NorthWest" ->
            JD.succeed NorthWest

        "North" ->
            JD.succeed North

        "NorthEast" ->
            JD.succeed NorthEast

        "West" ->
            JD.succeed West

        "Center" ->
            JD.succeed Center

        "East" ->
            JD.succeed East

        "SouthWest" ->
            JD.succeed SouthWest

        "South" ->
            JD.succeed South

        "SouthEast" ->
            JD.succeed SouthEast

        _ ->
            JD.fail "not a valid Field name"


fieldValueHelp : String -> JD.Decoder FieldValue
fieldValueHelp fieldValueString =
    case fieldValueString of
        "Cross" ->
            JD.succeed Cross

        "Nought" ->
            JD.succeed Nought

        _ ->
            JD.fail "not a valid FieldValue name"



-- Model


type alias Model =
    { moveList : List Move
    , previousGames : List (List Move)
    , currentPlayer : Maybe Player
    , now : Posix
    , lastMove : Posix
    , maxTimeMillis : PlayerTimes
    , ownSymbol : FieldValue
    , winningPlayer : Maybe Player
    }


urlToWebSocketString : Url.Url -> String
urlToWebSocketString url =
    let
        protocol =
            case url.protocol of
                Url.Http ->
                    "ws://"

                Url.Https ->
                    "wss://"
    in
    protocol ++ url.host ++ url.path


init : () -> Url.Url -> Browser.Navigation.Key -> ( Model, Cmd Msg )
init _ url _ =
    ( { moveList = []
      , previousGames = []
      , currentPlayer = Just Self
      , now = Time.millisToPosix 0
      , lastMove = Time.millisToPosix 0
      , maxTimeMillis =
            { self = 30000
            , other = 30000
            }
      , ownSymbol = Cross
      , winningPlayer = Nothing
      }
    , connectToSocket <| urlToWebSocketString url
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.batch
        [ Time.every 10 Tick
        , messageReceiver Recv
        ]


type FieldValue
    = Empty
    | Cross
    | Nought


type Field
    = NorthWest
    | North
    | NorthEast
    | West
    | Center
    | East
    | SouthWest
    | South
    | SouthEast


type alias Move =
    { field : Field
    , value : FieldValue
    , timeSpent : Int
    }


type alias BoardState =
    { northwest : FieldValue
    , north : FieldValue
    , northeast : FieldValue
    , west : FieldValue
    , center : FieldValue
    , east : FieldValue
    , southwest : FieldValue
    , south : FieldValue
    , southeast : FieldValue
    }


type Player
    = Self
    | Other


applyMove : Move -> BoardState -> BoardState
applyMove move current_state =
    case move.field of
        NorthWest ->
            { current_state | northwest = move.value }

        North ->
            { current_state | north = move.value }

        NorthEast ->
            { current_state | northeast = move.value }

        West ->
            { current_state | west = move.value }

        Center ->
            { current_state | center = move.value }

        East ->
            { current_state | east = move.value }

        SouthWest ->
            { current_state | southwest = move.value }

        South ->
            { current_state | south = move.value }

        SouthEast ->
            { current_state | southeast = move.value }


boardStateFromMoves : List Move -> BoardState
boardStateFromMoves moves =
    case moves of
        [] ->
            { northwest = Empty
            , north = Empty
            , northeast = Empty
            , west = Empty
            , center = Empty
            , east = Empty
            , southwest = Empty
            , south = Empty
            , southeast = Empty
            }

        first :: rest ->
            applyMove first <| boardStateFromMoves rest


type Msg
    = Tick Time.Posix
    | GridSquareClicked Field
    | Noop
    | Recv String
    | SwitchSymbol


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick newTime ->
            let
                boardstate =
                    boardStateFromMoves model.moveList

                winningSymbol =
                    winner boardstate

                gameDone =
                    gameFinished boardstate
            in
            case winningSymbol of
                Just symbol ->
                    if model.ownSymbol == symbol then
                        ( { model | winningPlayer = Just Self }, Cmd.none )

                    else
                        ( { model | winningPlayer = Just Other }, Cmd.none )

                Nothing ->
                    if gameDone then
                        ( { model
                            | moveList = []
                            , previousGames = model.previousGames ++ [ model.moveList ]
                            , maxTimeMillis =
                                timeLeftFromModel model
                            , ownSymbol =
                                if model.ownSymbol == Cross then
                                    Nought

                                else
                                    Cross
                            , winningPlayer = Nothing
                          }
                        , Cmd.none
                        )

                    else if model.moveList == [] && model.previousGames == [] then
                        ( { model | now = newTime, lastMove = newTime }, Cmd.none )

                    else
                        ( { model | now = newTime }, Cmd.none )

        SwitchSymbol ->
            let
                newSymbol =
                    if model.ownSymbol == Cross then
                        Nought

                    else
                        Cross

                newPlayer =
                    if model.currentPlayer == Just Self then
                        Just Other

                    else if model.currentPlayer == Just Other then
                        Just Self

                    else
                        model.currentPlayer
            in
            ( { model | ownSymbol = newSymbol, currentPlayer = newPlayer }, Cmd.none )

        GridSquareClicked field ->
            case model.currentPlayer of
                Just Self ->
                    let
                        moveMsg =
                            { field = field
                            , value = model.ownSymbol
                            , timeSpent =
                                Time.posixToMillis model.now - Time.posixToMillis model.lastMove
                            }
                    in
                    ( { model
                        | moveList = model.moveList ++ [ moveMsg ]
                        , currentPlayer = Just Other
                        , lastMove = model.now
                      }
                    , sendMessage <| moveEncoder moveMsg
                    )

                _ ->
                    ( model, Cmd.none )

        Recv moveString ->
            let
                moveDecoded =
                    JD.decodeString moveDecoder moveString
            in
            case moveDecoded of
                Ok move ->
                    ( { model
                        | moveList = model.moveList ++ [ move ]
                        , currentPlayer = Just Self
                        , lastMove = model.now
                        , ownSymbol =
                            case move.value of
                                Cross ->
                                    Nought

                                _ ->
                                    Cross
                      }
                    , Cmd.none
                    )

                Err _ ->
                    ( model, Cmd.none )

        _ ->
            ( model, Cmd.none )


textButton : Field -> FieldValue -> El.Element Msg
textButton field fieldValue =
    El.el
        []
    <|
        Input.button
            [ El.width (El.px 75)
            , El.height (El.px 75)
            , El.htmlAttribute <| Hat.style "box-shadow" "none"
            ]
            { onPress = Just (GridSquareClicked field)
            , label =
                center <|
                    El.html
                        (case fieldValue of
                            Cross ->
                                crossSvg

                            Nought ->
                                noughtSvg

                            Empty ->
                                Html.text ""
                        )
            }


switchButton : El.Element Msg
switchButton =
    center <|
        Input.button [ El.width (El.px 50), El.height (El.px 50) ]
            { onPress = Just SwitchSymbol
            , label = center <| El.text "switch symbol (debug)"
            }


whiteSquareColor : Color
whiteSquareColor =
    rgb255 240 217 181


blackSquareColor : Color
blackSquareColor =
    rgb255 181 136 99


textColor : Color
textColor =
    rgb255 186 186 186


backgroundColor : Color
backgroundColor =
    rgb255 23 21 18


elementColor : Color
elementColor =
    rgb255 38 36 33


redClockColor : Color
redClockColor =
    rgb255 121 44 42


greenClockColor : Color
greenClockColor =
    rgb255 57 72 35


fieldToColor : Field -> Color
fieldToColor field =
    case field of
        NorthWest ->
            blackSquareColor

        North ->
            whiteSquareColor

        NorthEast ->
            blackSquareColor

        West ->
            whiteSquareColor

        Center ->
            blackSquareColor

        East ->
            whiteSquareColor

        SouthWest ->
            blackSquareColor

        South ->
            whiteSquareColor

        SouthEast ->
            blackSquareColor


fieldValueToEl : Field -> FieldValue -> El.Element Msg
fieldValueToEl field value =
    El.el
        [ Background.color (fieldToColor field)
        , El.width El.fill
        , El.height El.fill
        ]
        (textButton field value)


viewGame : BoardState -> El.Element Msg
viewGame current_state =
    let
        row0 =
            El.row []
                [ fieldValueToEl NorthWest current_state.northwest
                , fieldValueToEl North current_state.north
                , fieldValueToEl NorthEast current_state.northeast
                ]

        row1 =
            El.row []
                [ fieldValueToEl West current_state.west
                , fieldValueToEl Center current_state.center
                , fieldValueToEl East current_state.east
                ]

        row2 =
            El.row []
                [ fieldValueToEl SouthWest current_state.southwest
                , fieldValueToEl South current_state.south
                , fieldValueToEl SouthEast current_state.southeast
                ]
    in
    El.row [ El.centerY, El.width El.fill, Border.rounded 7, clip ]
        [ El.column [ El.centerX, El.height El.fill ]
            [ row0, row1, row2 ]
        ]


center : El.Element msg -> El.Element msg
center element =
    El.el
        [ El.centerX, El.centerY ]
        element


type alias PlayerTimes =
    { self : Int
    , other : Int
    }


timeLeftFromModel : Model -> PlayerTimes
timeLeftFromModel model =
    case model.moveList of
        [] ->
            model.maxTimeMillis

        first :: rest ->
            let
                restTimes =
                    timeLeftFromModel { model | moveList = rest }
            in
            if first.value == model.ownSymbol then
                { restTimes | self = restTimes.self - first.timeSpent }

            else
                { restTimes | other = restTimes.other - first.timeSpent }


liveTimesFromModel : Model -> PlayerTimes
liveTimesFromModel model =
    let
        lastMoveTimes =
            timeLeftFromModel model

        timeSinceLastTurn =
            Time.posixToMillis model.now - Time.posixToMillis model.lastMove
    in
    case model.currentPlayer of
        Just Self ->
            { lastMoveTimes | self = lastMoveTimes.self - timeSinceLastTurn }

        Just Other ->
            { lastMoveTimes | other = lastMoveTimes.other - timeSinceLastTurn }

        Nothing ->
            lastMoveTimes


clockString : Int -> String
clockString millis =
    let
        seconds =
            String.fromInt (millis // 1000)

        secondsFormatted =
            String.padLeft 2 '0' seconds

        tenths =
            String.fromInt (modBy 1000 millis // 100)
    in
    "00:"
        ++ secondsFormatted
        ++ (if millis < 10000 then
                "." ++ tenths

            else
                ""
           )


clock : Player -> PlayerTimes -> Maybe Player -> El.Element Msg
clock player playerTimes currentPlayer =
    let
        playerTime =
            if player == Self then
                playerTimes.self

            else
                playerTimes.other
    in
    El.el
        [ Font.size 50
        , Font.color textColor
        , El.htmlAttribute <| Hat.style "min-width" "50%"
        , El.htmlAttribute <| Hat.style "left" "3%"
        , El.htmlAttribute <| Hat.style "font-kerning" "none"
        , Background.color
            (if (Just player /= currentPlayer) || (playerTime == 30000) then
                elementColor

             else if playerTime < 10000 then
                redClockColor

             else
                greenClockColor
            )
        , El.paddingXY 20 2
        , Border.roundEach
            (case player of
                Self ->
                    { topLeft = 0
                    , topRight = 0
                    , bottomLeft = 7
                    , bottomRight = 7
                    }

                Other ->
                    { topLeft = 7
                    , topRight = 7
                    , bottomLeft = 0
                    , bottomRight = 0
                    }
            )
        ]
    <|
        El.text <|
            clockString <|
                max 0 playerTime


winScreen : Html Msg
winScreen =
    El.layout [] <| center <| El.text "YOU WON"


lossScreen : Html Msg
lossScreen =
    El.layout [] <| center <| El.text "YOU LOST"


view : Model -> Browser.Document Msg
view model =
    { title =
        case model.currentPlayer of
            Just Self ->
                "Your turn - tictactoe"

            Just Other ->
                "Waiting for opponent - tictactoe"

            Nothing ->
                "tictactoe"
    , body =
        [ case model.winningPlayer of
            Just player ->
                if player == Self then
                    winScreen

                else
                    lossScreen

            Nothing ->
                let
                    boardState =
                        boardStateFromMoves model.moveList

                    playerTimes =
                        liveTimesFromModel model
                in
                if playerTimes.self < 0 then
                    lossScreen

                else if playerTimes.other < -2000 then
                    winScreen

                else
                    El.layout [ Background.color (rgb255 23 21 18) ] <|
                        center <|
                            El.column []
                                [ clock Other playerTimes model.currentPlayer
                                , viewGame boardState
                                , clock Self playerTimes model.currentPlayer
                                ]
        ]
    }



-- Game logic


lineFilledWith : FieldValue -> List FieldValue -> Bool
lineFilledWith value line =
    case line of
        symbol :: rest ->
            if symbol == value then
                lineFilledWith value rest

            else
                False

        [] ->
            True


gameFinished : BoardState -> Bool
gameFinished boardstate =
    let
        rows =
            [ [ boardstate.northwest, boardstate.north, boardstate.northeast ]
            , [ boardstate.west, boardstate.center, boardstate.east ]
            , [ boardstate.southwest, boardstate.south, boardstate.southeast ]
            ]
    in
    List.all
        (\row -> List.all (\el -> el /= Empty) row)
        rows


winner : BoardState -> Maybe FieldValue
winner boardstate =
    let
        lines =
            [ [ boardstate.northwest, boardstate.north, boardstate.northeast ]
            , [ boardstate.west, boardstate.center, boardstate.east ]
            , [ boardstate.southwest, boardstate.south, boardstate.southeast ]
            , [ boardstate.northwest, boardstate.west, boardstate.southwest ]
            , [ boardstate.north, boardstate.center, boardstate.south ]
            , [ boardstate.northeast, boardstate.east, boardstate.southeast ]
            , [ boardstate.northwest, boardstate.center, boardstate.southeast ]
            , [ boardstate.northeast, boardstate.center, boardstate.southwest ]
            ]

        crossWins =
            List.filterMap
                (\x ->
                    if x == [ Cross, Cross, Cross ] then
                        Just Cross

                    else
                        Nothing
                )
                lines

        noughtWins =
            List.filterMap
                (\x ->
                    if x == [ Nought, Nought, Nought ] then
                        Just Nought

                    else
                        Nothing
                )
                lines
    in
    if crossWins /= [] then
        Just Cross

    else if noughtWins /= [] then
        Just Nought

    else
        Nothing
