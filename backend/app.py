#!/usr/bin/env python3
from collections import defaultdict

from fastapi import FastAPI, WebSocket, Response
from fastapi.responses import HTMLResponse

from dirdict import DirDict


app = FastAPI()

with open("frontend/index.html") as index:
    html = index.read()

clients: dict[str, list] = defaultdict(list)


frontend_dir = DirDict("frontend")


@app.get("/")
async def get_root():
    return HTMLResponse(html)


@app.get("/{filename}")
async def get_file(filename: str):
    print([filename for filename in frontend_dir])
    if filename in frontend_dir and filename.endswith(".js"):
        return Response(
            content=frontend_dir[filename].decode(), media_type="text/javascript"
        )
    else:
        return HTMLResponse(html)


@app.websocket("/{path}")
async def websocket_endpoint(ws: WebSocket, path: str):
    await ws.accept()
    clients[path].append(ws)
    print(f"Player {len(clients[path])} joined on path {path}")
    while True:
        message = await ws.receive_text()
        remove_ws = []
        for otherws in clients[path]:
            if otherws == ws:
                continue
            try:
                print("trying to send message")
                await otherws.send_text(message)
            except:
                print("message failed to send")
                remove_ws.append(otherws)

        for r in remove_ws:
            clients[path].remove(r)
