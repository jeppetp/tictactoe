FROM node:latest

RUN npm install -g parcel-bundler @lydell/elm

WORKDIR /frontend/
COPY ./frontend/ /frontend/

RUN ls
RUN parcel build index.html

FROM python:3.11-alpine

COPY ./backend/requirements.txt .
RUN pip install -r requirements.txt

COPY ./backend .
COPY --from=0 /frontend/dist /frontend/ 

CMD uvicorn app:app --host 0.0.0.0 --port 80
